# syntax=docker/dockerfile:1.1.7-experimental
#############################################
# Tox testsuite for multiple python version #
#############################################
FROM advian/tox-base:alpine as tox
ARG PYTHON_VERSIONS="3.9.0 3.8.6 3.7.9"
ARG POETRY_VERSION="1.1.3"
RUN for pyver in $PYTHON_VERSIONS; do pyenv install -s $pyver; done \
    && pyenv global $PYTHON_VERSIONS \
    && poetry self update $POETRY_VERSION || pip install -U poetry==$POETRY_VERSION \
    && true


######################
# Base builder image #
######################
FROM python:3.7-alpine as builder_base

ENV \
  # locale
  LC_ALL=C.UTF-8 \
  # python:
  PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  # pip:
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  # poetry:
  POETRY_VERSION=1.1.4


RUN apk add --no-cache \
        curl \
        git \
        bash \
        build-base \
        libffi-dev \
        linux-headers \
        openssl \
        openssl-dev \
        zeromq \
        tini \
        openssh-client \
    # githublab ssh
    && mkdir -p -m 0700 ~/.ssh && ssh-keyscan gitlab.com github.com | sort > ~/.ssh/known_hosts \
    # Installing `poetry` package manager:
    # https://github.com/python-poetry/poetry
    #&& pip3 install "poetry==$POETRY_VERSION" \
    # Workaround problem with pypi poetry and datastreamservicelib requiring conflicting tomlkit
    # (would mess up the requirements.txt workaround below).
    && curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3 \
    && echo 'source $HOME/.poetry/env' >>/root/.profile \
    && source $HOME/.poetry/env \
    # We're in a container, do not create useless virtualenvs
    && poetry config virtualenvs.create false \
    && true

SHELL ["/bin/bash", "-lc"]


# Copy only requirements, to cache them in docker layer:
WORKDIR /pysetup
COPY ./poetry.lock ./pyproject.toml /pysetup/
# Install basic requirements
# But only after https://github.com/python-poetry/poetry/issues/2102 is fixed
#RUN --mount=type=ssh poetry config virtualenvs.create false \
#  && poetry install --no-dev --no-interaction --no-ansi
# Workaround by exporting deps, building and installing them via pip
RUN --mount=type=ssh pip3 install wheel \
    && poetry export -f requirements.txt --without-hashes -o /tmp/requirements.txt \
    && pip3 wheel --wheel-dir=/tmp/wheelhouse -r /tmp/requirements.txt \
    && pip3 install /tmp/wheelhouse/*.whl \
    && true


#####################################
# Base stage for development builds #
#####################################
FROM builder_base as devel_build
# Install deps
WORKDIR /pysetup
RUN --mount=type=ssh poetry install --no-interaction --no-ansi \
    && true

###########
# Hacking #
###########
FROM devel_build as devel_shell
# Copy everything to the image
COPY . /app
WORKDIR /app
RUN apk add --no-cache zsh \
    && sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" \
    && echo "source /root/.profile" >>/root/.zshrc \
    && pip3 install git-up \
    && true
ENTRYPOINT ["/bin/zsh", "-l"]
