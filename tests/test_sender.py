"""Test the sender"""
import io

import msgpacketizer
from msgpacketizer.packer import unpack


def test_sender_roundtrip() -> None:
    """Test the send and read back to decode"""
    stream = io.BytesIO()
    pkgidx = 255
    test_dict = {"ints": list(range(10)), "bytes": bytes(10)}

    # push the data to the bytesio object
    msgpacketizer.send(stream, pkgidx, test_dict)
    # read back and unpack
    packed = stream.getvalue()
    stream.close()
    unp_pkgidx, unp_dict = unpack(packed)
    assert unp_pkgidx == pkgidx
    for key in test_dict:
        assert key in unp_dict
        assert unp_dict[key] == test_dict[key]
