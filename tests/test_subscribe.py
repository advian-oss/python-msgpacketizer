"""Test subscription handling"""
from typing import Any, Optional
import io

from cobs import cobs  # type: ignore
import msgpacketizer
from msgpacketizer.packer import pack


def test_sub_added() -> None:
    """Test that subscription was added"""
    assert not msgpacketizer.subscriber.DEFAULT_TRACKER.subs_by_stream

    def callback(data: Any) -> None:
        """dummy callback"""
        _ = data

    stream = io.BytesIO()

    msgpacketizer.subscribe(stream, 255, callback)
    assert msgpacketizer.subscriber.DEFAULT_TRACKER.subs_by_stream
    ref = msgpacketizer.subscriber.DEFAULT_TRACKER.subs_by_stream
    assert stream in ref
    assert 255 in ref[stream]
    assert ref[stream][255]

    assert msgpacketizer.subscriber.DEFAULT_TRACKER.buffers_by_stream
    assert stream in msgpacketizer.subscriber.DEFAULT_TRACKER.buffers_by_stream


def test_single_callback() -> None:
    """Test that a single callbck gets fired"""
    assert not msgpacketizer.subscriber.DEFAULT_TRACKER.subs_by_stream
    # pylint: disable=E1136,E1135

    received_data: Optional[Any] = None

    def callback(data: Any) -> None:
        """Just put the data back to the outer scope"""
        nonlocal received_data
        received_data = data

    stream = io.BytesIO()
    pkgidx = 255

    # Subscribe
    msgpacketizer.subscribe(stream, pkgidx, callback)

    # push the data to the stream and seek back for parsing
    test_dict = {"ints": list(range(10)), "bytes": bytes(10)}
    msgpacketizer.send(stream, pkgidx, test_dict)
    stream.seek(0)

    # Call the parser
    msgpacketizer.parse()
    # Make sure we got the packet
    assert received_data
    for key in test_dict:
        assert key in received_data
        assert received_data[key] == test_dict[key]


def test_wrong_idx() -> None:
    """Make sure wrong idx message does not fire callback"""
    assert not msgpacketizer.subscriber.DEFAULT_TRACKER.subs_by_stream
    # pylint: disable=E1136,E1135

    received_data: Optional[Any] = None

    def callback(data: Any) -> None:
        """Just put the data back to the outer scope"""
        nonlocal received_data
        received_data = data

    stream = io.BytesIO()
    pkgidx = 255

    # Subscribe
    msgpacketizer.subscribe(stream, pkgidx, callback)

    # push the data to the stream and seek back for parsing
    msgpacketizer.send(stream, pkgidx ^ 0x1, {"foo": "bar"})
    stream.seek(0)

    # Call the parser
    msgpacketizer.parse()
    # Make sure we got no packet
    assert received_data is None

    # And make sure normal packet handling works for correct idx
    stream.seek(0)
    stream.truncate()
    test_dict = {"ints": list(range(10)), "bytes": bytes(10)}
    msgpacketizer.send(stream, pkgidx, test_dict)

    stream.seek(0)
    msgpacketizer.parse()
    assert received_data
    for key in test_dict:
        assert key in received_data
        assert received_data[key] == test_dict[key]


def test_corrupted_packets() -> None:
    """make sure corrupted packets are ignored"""
    assert not msgpacketizer.subscriber.DEFAULT_TRACKER.subs_by_stream
    # pylint: disable=E1136,E1135

    received_data: Optional[Any] = None

    def callback(data: Any) -> None:
        """Just put the data back to the outer scope"""
        nonlocal received_data
        received_data = data

    stream = io.BytesIO()
    pkgidx = 255

    def clear_stream() -> None:
        nonlocal stream
        stream.seek(0)
        stream.truncate()
        # This should not be needed though...
        # msgpacketizer.subscriber.DEFAULT_TRACKER.buffers_by_stream[stream] = b""

    # Subscribe
    msgpacketizer.subscribe(stream, pkgidx, callback)

    # push the data to the stream
    test_dict = {"ints": list(range(10)), "bytes": bytes(10)}
    msgpacketizer.send(stream, pkgidx, test_dict)

    # insert stray null
    stream.seek(6)
    stream.write(b"\0")

    # Call the parser
    stream.seek(0)
    msgpacketizer.parse()
    # Make sure we got no packet
    assert received_data is None

    # Make a checksum failure
    clear_stream()
    packed = pack(pkgidx, test_dict)
    cs_corrupted = list(cobs.decode(packed))
    cs_corrupted[-1] = cs_corrupted[-1] ^ 0x1  # a bit to mess the checksum
    stream.write(cobs.encode(bytes(cs_corrupted) + b"\0"))

    # Call the parser
    stream.seek(0)
    msgpacketizer.parse()
    # Make sure we got no packet
    assert received_data is None

    # Make sure we can still get a valid packet
    clear_stream()
    test_dict = {"ints": list(range(10)), "bytes": bytes(10)}
    msgpacketizer.send(stream, pkgidx, test_dict)

    # Call the parser
    stream.seek(0)
    msgpacketizer.parse()
    # Make sure we got a packet
    assert received_data
