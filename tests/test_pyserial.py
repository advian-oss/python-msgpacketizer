"""Test with pyserial loopback"""
from typing import Any, Optional

import serial  # type: ignore
import msgpacketizer


def test_roundtrip() -> None:
    """Test roundtrip on loopback stream"""
    assert not msgpacketizer.subscriber.DEFAULT_TRACKER.subs_by_stream
    # pylint: disable=E1136,E1135
    stream = serial.serial_for_url("loop:///testloop", timeout=0.01)
    received_data: Optional[Any] = None
    pkgidx = 255

    def callback(data: Any) -> None:
        """Just put the data back to the outer scope"""
        nonlocal received_data
        received_data = data

    # Subscribe
    msgpacketizer.subscribe(stream, pkgidx, callback)
    # push the data to the stream
    test_dict = {"ints": list(range(10)), "bytes": bytes(10)}
    msgpacketizer.send(stream, pkgidx, test_dict)

    # Call the parser
    msgpacketizer.parse()
    # Make sure we got the packet
    assert received_data
    for key in test_dict:
        assert key in received_data
        assert received_data[key] == test_dict[key]
