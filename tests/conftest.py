"""default fixtures"""
import pytest  # type: ignore

import msgpacketizer.subscriber


@pytest.fixture(autouse=True)
def clean_default_tracker():  # type: ignore
    """Make sure we have clean magical default tracker every time"""
    tracker_backup = msgpacketizer.subscriber.DEFAULT_TRACKER
    msgpacketizer.subscriber.DEFAULT_TRACKER = msgpacketizer.subscriber.SubscriptionTracker()
    yield msgpacketizer.subscriber.DEFAULT_TRACKER
    msgpacketizer.subscriber.DEFAULT_TRACKER = tracker_backup
