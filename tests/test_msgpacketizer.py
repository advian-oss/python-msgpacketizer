"""Package level tests"""
import io


import pytest  # type: ignore
from msgpacketizer import __version__
import msgpacketizer


def test_version() -> None:
    """Make sure version matches expected"""
    assert __version__ == "0.2.0"


def test_post() -> None:
    """Test the post method"""
    msgpacketizer.post()


def test_publish() -> None:
    """Make sure publish raises error"""
    stream = io.BytesIO()
    pkgidx = 255
    test_dict = {"ints": list(range(10)), "bytes": bytes(10)}
    with pytest.raises(NotImplementedError):
        msgpacketizer.publish(stream, pkgidx, test_dict)
