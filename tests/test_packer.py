"""Test the binary pack/unpack"""
import pytest  # type: ignore
from cobs import cobs  # type: ignore

from msgpacketizer.packer import pack, unpack


def test_roundtrip() -> None:
    """Test the pack/unpack roundtrip"""
    pkgidx = 255
    pack_dict = {"ints": list(range(10)), "bytes": bytes(10)}
    packed = pack(pkgidx, pack_dict)
    unp_pkgidx, unp_dict = unpack(packed)
    assert unp_pkgidx == pkgidx
    for key in pack_dict:
        assert key in unp_dict
        assert unp_dict[key] == pack_dict[key]


def test_corrupted_roundtrip() -> None:
    """Test the pack/unpack roundtrip"""
    pkgidx = 255
    pack_dict = {"ints": list(range(10)), "bytes": bytes(10)}
    packed = pack(pkgidx, pack_dict)

    # convert to list so we can mess with the internals
    cobs_corrupted = list(packed)
    cobs_corrupted[3] = 0
    with pytest.raises(cobs.DecodeError):
        _ = unpack(bytes(cobs_corrupted))
    del cobs_corrupted

    # decode cobs to list so we can mess with the internals more
    cs_corrupted = list(cobs.decode(packed))
    cs_corrupted[-1] = cs_corrupted[-1] ^ 0x1  # flip a bit to mess the checksum
    with pytest.raises(ValueError):
        _ = unpack(cobs.encode(bytes(cs_corrupted)))
    del cs_corrupted


def test_roundtrip_trailingnull() -> None:
    """Test the pack/unpack roundtrip with trailing null in the packed data"""
    pkgidx = 255
    pack_dict = {"ints": list(range(10)), "bytes": bytes(10)}
    packed = pack(pkgidx, pack_dict) + b"\0"
    unp_pkgidx, unp_dict = unpack(packed)
    assert unp_pkgidx == pkgidx
    for key in pack_dict:
        assert key in unp_dict
        assert unp_dict[key] == pack_dict[key]


def test_pack_invalid_idx() -> None:
    """Test invalid pkgidx values"""
    with pytest.raises(ValueError):
        pack_dict = {"ints": list(range(10)), "bytes": bytes(10)}
        _ = pack(2.5, pack_dict)  # type: ignore

    with pytest.raises(ValueError):
        pack_dict = {"ints": list(range(10)), "bytes": bytes(10)}
        _ = pack(b"foo", pack_dict)

    with pytest.raises(ValueError):
        pack_dict = {"ints": list(range(10)), "bytes": bytes(10)}
        _ = pack(b"", pack_dict)

    with pytest.raises(ValueError):
        pack_dict = {"ints": list(range(10)), "bytes": bytes(10)}
        _ = pack(300, pack_dict)
